# 10minutes

Repo supplementary to the video [Make your game in 10 minutes with Godot](url)

- [10minutes](#10minutes)
  - [Input Map Cheat Sheet](#input-map-cheat-sheet)
  - [Important Links](#important-links)
    - [Godot](#godot)
    - [Boiling Steam](#boiling-steam)



## Input Map Cheat Sheet

| Action Name       | Physical Key* | Key             | Joy Button          | Joy Axis           | Mouse Button           |
| :---------------- | :----------- | :-------------- | :------------------ | :----------------- | :--------------------- |
| move_left         | A            | &#x2B05;        | Device 0, Button 14 | Device 0, Axis 0 - |
| move_right        | D            | &#x27A1;        | Device 0, Button 15 | Device 0, Axis 0 + |
| move_forward      | W            | &#x2B06;        | Device 0, Button 12 | Device 0, Axis 1 - |
| move_back         | S            | &#x2B07;        | Device 0, Button 13 | Device 0, Axis 1 + |
| jump              |              | Space           | Device 0, Button 0  |                    |
| aim               |              |                 | Device 0, Button 6  |                    | Device 0, Right Button |
| shoot             |              |                 | Device 0, Button 7  |                    | Device 0, Left Button  |
| quit              |              | Escape          | Device 0, Button 1  |                    |
| view_left         |              |                 |                     | Device 0, Axis 2 - |
| view_right        |              |                 |                     | Device 0, Axis 2 + |
| view_down         |              |                 |                     | Device 0, Axis 3 - |
| view_up           |              |                 |                     | Device 0, Axis 3 + |
| toggle_fullscreen |              | F11 / Alt+Enter | Device 0, Button 1  |                    |
| toggle_debug      |              | F3              | Device 0, Button 1  |                    |

> * [**Physical Keys**](https://docs.godotengine.org/en/stable/classes/class_input.html?highlight=physical%20keys#class-input-method-is-physical-key-pressed) is the "position" of it on the keyboard. For example, If I map WASD on a QWERTY keyboard, it will remap to ZQSD if you change your keyboard to AZERTY. Key is the exact key on the keyboard.


## Important Links

### Godot
- 📼 [Make your game in 10 minutes with Godot - Video](url) *Pending
- 🧾 [Make your game in 10 minutes with Godot - Article](url) *Pending
- 🤖 [Godot Engine](https://godotengine.org/)
- 💾 [Godot Download](https://godotengine.org/download)
- 📜 [Godot Documentation / Learn](https://docs.godotengine.org/en/stable/)
- 🎮 [Godot TPS Demo](https://github.com/godotengine/tps-demo)
- 🕹️ [Godot Demos](https://godotengine.github.io/godot-demo-projects/)

---

### Boiling Steam

- 🐧 [Boiling Steam](https://boilingsteam.com/) 
- 🐦 [Twitter](https://twitter.com/BoilingSteam)
- 🦣 [Mastodon](https://mastodon.cloud/@boilingsteam) 
- 🦊 [GitLab](https://gitlab.com/boiling_steam)
- 🌐 [Matrix](https://matrix.to/#/#boilingsteam:matrix.org) 
- 📼 [YouTube](https://www.youtube.com/channel/UCeQ36uE1kgT3mHHHdhEyK_w) 
- 👥 [Peertube](https://peertube.linuxrocks.online/c/boilingsteam/videos)
- ⛵ [Odysee](https://odysee.com/@boilingsteam:8)



